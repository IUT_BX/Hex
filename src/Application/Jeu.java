package Application;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import Hex.Click;
import Hex.Controleur;
import Hex.Modele;
import Hex.VueJeu;

/**
 * M�thode principale. Appelle le mod�de, la vue et le controlleur.
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Jeu {

	public static void main(String[] args) {
		// On d�finit le theme windows
	
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				e.printStackTrace();
			}
	

		Modele modele = new Modele();
		Controleur controleur = new Controleur(modele);

		VueJeu vueJeu = new VueJeu(modele);
		modele.addObserver(vueJeu);

		Click click = new Click(controleur);
		vueJeu.addMouseListener(click);
	}

}
