package Exception;

/**
 * Exception g�rant une taille de plateau invalide
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class TaillePlateauInvalideException extends Exception {
	private static final long serialVersionUID = 1L;

	public TaillePlateauInvalideException() {
		super("La taille du plateau de jeu est invalide");
	}
}
