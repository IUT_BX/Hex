package Hex;

import Exception.TaillePlateauInvalideException;

/**
 * Plateau de jeu
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Plateau {
	private int nbLignes;
	private int nbColonnes;
	/**
	 * Plateau de jeu contenant les cellules
	 */
	private Cellule[][] cellules;

	public Plateau(int nbLignes, int nbColonnes) throws Exception {
		if (nbLignes <= 0 || nbColonnes <= 0) {
			throw new TaillePlateauInvalideException();
		}
		this.nbLignes = nbLignes;
		this.nbColonnes = nbColonnes;
		this.cellules = new Cellule[nbColonnes][nbLignes];
		initialiser(nbLignes, nbColonnes);
	}

	/**
	 * Initalise le plateau avec des cases vides
	 * 
	 * @param nbLignes
	 * @param nbColonnes
	 */
	private void initialiser(int nbLignes, int nbColonnes) {
		int j;
		Cellule tempCellule = null;
		for (int i = 0; i < nbColonnes; i++) {
			for (j = 0; j < nbLignes; j++) {
				float x = (float) (i * 2 * Cellule.rayon) + 90;
				float y = (float) (j * 2 * Cellule.rayon - i * Cellule.rayon) + 190;
				tempCellule = new Cellule(i, j);
				tempCellule.setX(x);
				tempCellule.setY(y);
				tempCellule.creerPolygone();
				this.cellules[i][j] = tempCellule;
			}
		}
	}

	public int getNbLignes() {
		return nbLignes;
	}

	public int getNbColonnes() {
		return nbColonnes;
	}

	public Cellule getCellule(int i, int j) {
		return cellules[i][j];
	}

	public void reinitialiser() {
		initialiser(nbLignes, nbColonnes);
	}
}
