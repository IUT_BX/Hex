package Hex;

import java.awt.Color;
import java.awt.Polygon;

/**
 * D�finit une Cellule du jeu
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Cellule extends Polygon {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Abscisse en pixels du centre de la cellule
	 */
	private float centreX;
	/**
	 * Ordonnee en pixels du centre de la cellule
	 */
	private float centreY;

	/**
	 * Premier indice de la cellule (abscisses)
	 */
	private int posI;
	/**
	 * Deuxieme indice de la cellule (ordonnees)
	 */
	private int posJ;
	/**
	 * Couleur de la cellule
	 */
	private Color couleur;
	/**
	 * Rayon des cellules
	 */
	public static int rayon = 20;

	public Cellule(int posI, int posJ) {
		this.posI = posI;
		this.posJ = posJ;
		couleur = Constante.couleurCaseParDefaut;
	}

	/**
	 * Regarde tout autour du polygone de la cellule courante pour voir si elle
	 * est voisine avec la cellule passee en parametres
	 * 
	 * @param cellule
	 *            : cellule qui doit etre comparee avec la cellule courante
	 * @return Renvoie true si la cellule courante est voisine avec la cellule
	 *         passee en parametres, false sinon
	 */
	public boolean estVoisine(Cellule cellule) {
		int haut = this.getPosJ() - 1;
		int gauche = this.getPosI() - 1;
		int bas = this.getPosJ() + 1;
		int droite = this.getPosI() + 1;

		if (haut >= 0) {
			if (gauche >= 0) {
				// Si la cellule passee en parametres est en haut a gauche de la
				// cellule courante
				if (cellule.getPosI() == gauche && cellule.getPosJ() == haut) {
					return true;
				}
			}
			// Si la cellule passee en parametres est en haut de la cellule
			// courante
			if (cellule.getPosI() == this.getPosI()
					&& cellule.getPosJ() == haut) {
				return true;
			}
		}

		if (bas < Constante.nbColonnesPlateau) {
			// Si la cellule passee en parametres est en bas a droite de la
			// cellule courante
			if (droite < Constante.nbColonnesPlateau) {
				if (cellule.getPosI() == droite && cellule.getPosJ() == bas) {
					return true;
				}
			}
			// Si la cellule passee en parametres est en bas de la cellule
			// courante
			if (cellule.getPosI() == this.getPosI() && cellule.getPosJ() == bas) {
				return true;
			}
		}
		if (gauche >= 0) {
			if (gauche >= 0) {
				// Si la cellule passee en parametres est a gauche de la cellule
				// courante
				if (cellule.getPosI() == gauche
						&& cellule.getPosJ() == this.getPosJ()) {
					return true;
				}
			}
		}
		if (droite < Constante.nbColonnesPlateau) {
			// Si la cellule passee en parametres est a droite de la cellule
			// courante
			if (cellule.getPosI() == droite
					&& cellule.getPosJ() == this.getPosJ()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Cree un polygone a l'aide des coordonnees du centre de la cellule
	 */
	public void creerPolygone() {
		double arc = (Math.PI * 2) / 6;
		for (int i = 0; i <= 6; i++) {
			this.addPoint(
					(int) Math.round(centreX + Cellule.rayon
							* Math.cos(arc * i)),
					(int) Math.round(centreY + Cellule.rayon
							* Math.sin(arc * i)));
		}
	}

	public int getPosI() {
		return posI;
	}

	public int getPosJ() {
		return posJ;
	}

	public Color getCouleur() {
		return couleur;
	}

	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

	public void setX(float x) {
		this.centreX = x;
	}

	public void setY(float y) {
		this.centreY = y;
	}

}