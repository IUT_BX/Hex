package Hex;

import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputAdapter;

/**
 * Classe Click <br/>
 * Utilisee pour gerer les clics dans la vue
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Click extends MouseInputAdapter {
	private Controleur controller;

	public Click(Controleur controller) {
		this.controller = controller;
	}

	public void mouseClicked(MouseEvent click) {
		controller.Action(click);
	}
}
