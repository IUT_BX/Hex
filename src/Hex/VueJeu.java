package Hex;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Vue du jeu, d�termine l'affichage de l'application.
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class VueJeu extends Vue {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Cr�� la fenetre
	 * 
	 * @param model
	 */
	public VueJeu(Modele model) {
		super(model);
		jFrame = new JFrame("Hex Mompriv� Poussard S3B");
		jFrame.setSize(Constante.largeurFenetre, Constante.hauteurFenetre);
		jFrame.getContentPane().setBackground(Color.WHITE);
		jFrame.add(this);
		jFrame.setVisible(true);
		jFrame.setResizable(false);
	}

	/**
	 * Dessine le plateau de jeu
	 */
	public void paint(Graphics g) {
		super.paint(g);

		Polygon polygoneADessiner = null;
		Plateau plateau = getModel().getPlateau();
		for (int i = 0; i < plateau.getNbColonnes(); i++) {
			for (int j = 0; j < plateau.getNbLignes(); j++) {
				polygoneADessiner = plateau.getCellule(i, j);
				g.setColor(plateau.getCellule(i, j).getCouleur());
				g.fillPolygon(polygoneADessiner);
			}
		}
		int largeurTrait = 6;

		g.setColor(Constante.couleurJoueur1);
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(largeurTrait));

		g2.drawLine(largeurTrait / 2, 0, largeurTrait / 2,
				Constante.hauteurFenetre);
		g2.drawLine(Constante.largeurFenetre - largeurTrait * 2, 0,
				Constante.largeurFenetre - largeurTrait * 2,
				Constante.hauteurFenetre);

		g.setColor(Constante.couleurJoueur2);
		g2.drawLine(0, largeurTrait / 2, Constante.largeurFenetre,
				largeurTrait / 2);
		g2.drawLine(0, Constante.hauteurFenetre - largeurTrait * 5,
				Constante.largeurFenetre, Constante.hauteurFenetre
						- largeurTrait * 5);
	}

	/**
	 * Met � jour l'affichage du plateau (affiche la grille et, si besoin, la
	 * popup de fin de partie)
	 */
	@Override
	public void update(Observable o, Object arg) {

		if (arg != null) {
			String messageGagnant = "";
			if (arg instanceof EvenementsJeu) {
				Object[] optionsFinPartie = { "Rejouer", "Quitter" };

				switch ((EvenementsJeu) arg) {
				case J1GAGNE:
					messageGagnant = "Les bleus ont gagn�";
					break;
				case J2GAGNE:
					messageGagnant = "Les rouges ont gagn�";
					break;
				}

				int choix = JOptionPane.showOptionDialog(this.jFrame,
						messageGagnant + "\n Que voulez-vous faire ?",
						"Victoire !", JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, optionsFinPartie,
						optionsFinPartie[0]);

				switch (choix) {
				case 0:
					this.getModel().rejouer();
					break;
				case 1:
					this.jFrame.dispose();
					break;
				default:
					break;
				}
			}
		}

		this.repaint();
	}
}
