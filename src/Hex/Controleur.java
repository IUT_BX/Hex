package Hex;

import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 * Controleur du mod�le MVC
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Controleur {
	private Modele modele;

	public Controleur(Modele modele) {
		this.modele = modele;
	}

	/**
	 * D�finit ce que doit faire le modele lorsque l'utilisateur clique dans la
	 * fenetre
	 * 
	 * @param event
	 */
	public void Action(MouseEvent event) {
		switch (event.getID()) {
		case MouseEvent.MOUSE_CLICKED:
			Point point = new Point(event.getX(), event.getY());
			if (modele.jouerSiCaseVide(point)) {
				modele.verifierFinDePartie();
			}
			break;
		}

	}
}
