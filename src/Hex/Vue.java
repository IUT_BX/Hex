package Hex;

import java.awt.Canvas;
import java.util.Observer;

import javax.swing.JFrame;

/**
 * Vue du mod�le
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public abstract class Vue extends Canvas implements Observer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private Modele model;
	protected JFrame jFrame;

	public Vue(Modele model) {
		this.model = model;
	}

	public Modele getModel() {
		return model;
	}
}
