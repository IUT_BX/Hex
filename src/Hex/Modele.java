package Hex;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Observable;

/**
 * Classe Modele
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Modele extends Observable {
	/**
	 * Plateau de jeu contenant les cellules
	 */
	private Plateau plateau;
	/**
	 * Liste contenant les joueurs pouvant int�ragir avec le plateau
	 */
	private ArrayList<Joueur> joueurs;

	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	/**
	 * Num�ro du joueur qui doit poser un pion (varie entre 0 et 1)
	 */
	private int numeroJoueurCourant = 0;

	/**
	 * Objet contenant le joueur qui doit poser un pion
	 */
	private Joueur joueurCourant;

	/**
	 * Constructeur de Modele
	 */
	public Modele() {
		joueurs = new ArrayList<Joueur>();
		this.joueurs.add(new Joueur(Constante.couleurJoueur1));
		this.joueurs.add(new Joueur(Constante.couleurJoueur2));
		joueurCourant = joueurs.get(0);
		try {
			plateau = new Plateau(Constante.nbLignesPlateau,
					Constante.nbColonnesPlateau);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setPlateau(Plateau plate) {
		this.plateau = plate;
	}

	public Plateau getPlateau() {
		return plateau;
	}

	/**
	 * Change le num�ro du joueur suivant pour que ce dernier puisse poser un
	 * pion lors du prochain tour
	 */
	public void joueurSuivant() {
		// Augmente le numero du joueur qui doit poser un pion
		numeroJoueurCourant++;
		// Si ce numero depasse le nombre de joueurs, il revient a zero
		if (numeroJoueurCourant >= joueurs.size()) {
			numeroJoueurCourant = 0;
		}
		// D�finit la variable contneant le joueur qui doit poser un pion lors
		// du prochain tour
		joueurCourant = joueurs.get(numeroJoueurCourant);
	}

	/**
	 * Pose un pion sur les coordonnees placees en parametres
	 * 
	 * @param coordonnees
	 *            : Coordonnees de la souris
	 * @return Renvoie true si un pion a ete pose, false sinon
	 */
	public boolean jouerSiCaseVide(Point coordonnees) {
		Cellule cellule = null;
		boolean resultat = false;
		// On parcourt la liste des cases du plateau
		int i = 0;
		int j = 0;
		while (i < plateau.getNbColonnes() && !resultat) {
			while (j < plateau.getNbLignes() && !resultat) {
				// On r�cup�re chaque cellule une par une
				cellule = plateau.getCellule(i, j);
				// Si la cellule du plateau se situe dans les coordonnees de la
				// souris et qu'elle n'est pas coloree
				if (cellule.contains(coordonnees)
						&& cellule.getCouleur() == Constante.couleurCaseParDefaut) {
					// On colore la cellule
					cellule.setCouleur(joueurCourant.getCouleur());
					// On actualise les iles des joueurs
					joueurCourant.ajouterCelluleDansIle(cellule);
					// On passe au joueur suivant
					joueurSuivant();
					// On avertit la vue qu'on a modifie le modele
					this.setChanged();
					this.notifyObservers();
					resultat = true;
				}
				j++;
			}
			j = 0;
			i++;
		}
		return resultat;
	}

	/**
	 * Verifie si un joueur a gagne et avertit le modele si cela est le cas
	 */
	public void verifierFinDePartie() {
		// On regarde si un joueur a gagne
		Joueur gagnant = quiGagne();
		// Si cela est le cas, on regarde qui a gagne
		if (gagnant != null) {
			// On recupere la couleur du joueur gagnant
			Color couleurGagnant = gagnant.getCouleur();

			// Si le gagnant a la couleur 1, on demande a la vue d'afficher un
			// message de fin
			if (couleurGagnant == Constante.couleurJoueur1) {
				EvenementsJeu event = EvenementsJeu.J1GAGNE;
				this.setChanged();
				this.notifyObservers(event);
			}

			// Si le gagnant a la couleur 2, on demande a la vue d'afficher un
			// message de fin
			if (couleurGagnant == Constante.couleurJoueur2) {
				EvenementsJeu event = EvenementsJeu.J2GAGNE;
				this.setChanged();
				this.notifyObservers(event);
			}
		}
	}

	/**
	 * Determine si un joueur a gagne
	 * 
	 * @return Renvoie l'objet contenant le joueur gagnant s'il existe, null
	 *         sinon
	 */
	public Joueur quiGagne() {
		boolean debut = false;
		boolean fin = false;
		// On recupere le joueur 1
		Joueur joueur = joueurs.get(0);

		// On parcourt les iles du joueur
		for (Ile ile : joueur.getIles()) {
			// Si l'ile n'est pas reliee a gauche et a droite
			if (!debut || !fin) {
				// On remet a zero les deux booleens pour la prochaine ile
				debut = false;
				fin = false;
				// On parcourt les cellules de l'ile
				for (Cellule cellule : ile.getContenu()) {
					switch (cellule.getPosI()) {
					// Si la cellule est tout a gauche
					case 0:
						debut = true;
						break;
					// Si la cellule est tout a droite
					case Constante.nbColonnesPlateau - 1:
						fin = true;
						break;
					}
				}
			}
		}
		// Si le joueur 1 gagne, on retourne un objet qui le contient
		if (debut && fin) {
			return joueur;
		}
		// Si le joueur 1 n'a pas gagne, on teste si le joueur 2 a gagne
		else {
			// On recupere le joueur 2
			joueur = joueurs.get(1);

			// On parcourt les iles du joueur
			for (Ile ile : joueur.getIles()) {
				// Si l'ile n'est pas reliee en haut et en bas
				if (!debut || !fin) {
					// On remet a zero les deux booleens pour la prochaine ile
					debut = false;
					fin = false;
					// On parcourt les cellules de l'ile
					for (Cellule cellule : ile.getContenu()) {
						switch (cellule.getPosJ()) {
						// Si la cellule est tout en haut
						case 0:
							debut = true;
							break;
						// Si la cellule est tout en bas
						case Constante.nbLignesPlateau - 1:
							fin = true;
							break;
						}
					}
				}
			}
			// Si le joueur 1 gagne, on retourne un objet qui le contient
			if (debut && fin) {
				return joueur;
			}
		}
		// Sinon, on retourne null pour prevenir le modele qu'aucun joueur n'a
		// gagne
		return null;
	}

	public int getNumeroJoueurCourant() {
		return numeroJoueurCourant;
	}

	public Joueur getJoueurCourant() {
		return joueurCourant;
	}

	public void rejouer() {
		this.plateau.reinitialiser();
		for (Joueur j : this.joueurs) {
			j.getIles().clear();
		}
	}
}
