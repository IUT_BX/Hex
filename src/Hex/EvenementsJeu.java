package Hex;

/**
 * Evenements possibles de fin de jeu
 * 
 * @author Antoine Poussard & Remi Momprive (S3B)
 *
 */
public enum EvenementsJeu {
	/**
	 * Evenement declenche lorsque le joueur 1 (en largeur) gagne
	 */
	J1GAGNE,
	/**
	 * Evenement declenche lorsque le joueur 2 (en hauteur) gagne
	 */
	J2GAGNE
}
