package Hex;

import java.util.ArrayList;

/**
 * Classe Ile Cette classe permet de gerer les iles du joueur. Une ile est une
 * succession de cellules.
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Ile {
	/**
	 * ArrayList de cellules définissant une Ile
	 */
	private ArrayList<Cellule> contenu;

	public Ile() {
		contenu = new ArrayList<Cellule>();
	}

	public Ile(Cellule cellule) {
		contenu = new ArrayList<Cellule>();
		contenu.add(cellule);
	}

	public ArrayList<Cellule> getContenu() {
		return contenu;
	}

	public void setContenu(ArrayList<Cellule> contenu) {
		this.contenu = contenu;
	}

	/**
	 * Ajoute une Cellule
	 * 
	 * @param cellule
	 */
	public void ajouterCellule(Cellule cellule) {
		contenu.add(cellule);
	}

	/**
	 * Regarde si l'ArrayList contient cette cellule
	 * 
	 * @param cellule
	 * @return
	 */
	public boolean contient(Cellule cellule) {
		return contenu.contains(cellule);
	}
}
