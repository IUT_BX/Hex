package Hex;

import java.awt.Color;

/**
 * Int�gralit� des variables Constantes du Jeu
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Constante {
	/**
	 * Couleur par d�faut d'une cellule
	 */
	public static final Color couleurCaseParDefaut = Color.BLACK;
	/**
	 * Couleur du joueur 1 (en largeur)
	 */
	public static final Color couleurJoueur1 = Color.BLUE;
	/**
	 * Couleur du joueur 2 (en hauteur)
	 */
	public static final Color couleurJoueur2 = Color.RED;
	/**
	 * Hauteur du plateau
	 */
	public static final int nbLignesPlateau = 8;
	/**
	 * Largeur du plateau
	 */
	public static final int nbColonnesPlateau = 8;
	/**
	 * Largeur de la fen�tre de jeu
	 */
	public static final int largeurFenetre = 480;
	/**
	 * Hauteur de la fen�tre de jeu
	 */
	public static final int hauteurFenetre = 550;
}
