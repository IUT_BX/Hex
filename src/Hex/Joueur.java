package Hex;

import java.awt.Color;
import java.util.ArrayList;

/**
 * Classe Joueur, contient une ArrayList d'ile, g�re les iles de chaque joueur.
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class Joueur {
	/**
	 * ArrayList d'iles
	 */
	private ArrayList<Ile> iles;
	/**
	 * Couleur du Joueur
	 */

	private Color couleur;

	public Joueur(Color color) {
		this.couleur = color;
		iles = new ArrayList<Ile>();
	}

	/**
	 * Attache un pion � une ile voisine si cette derniere existe. Ajoute une
	 * nouvelle Ile � l'arrayList sinon.
	 * 
	 * @param cellule
	 */
	public void ajouterCelluleDansIle(Cellule cellule) {
		ArrayList<Cellule> tmpCellule = new ArrayList<Cellule>();
		ArrayList<Ile> ilesVoisines = new ArrayList<Ile>();
		for (Ile ile2 : iles) {
			tmpCellule = ile2.getContenu();
			for (Cellule cell2 : tmpCellule) {
				if (cell2.estVoisine(cellule) && !ilesVoisines.contains(ile2)) {
					ilesVoisines.add(ile2);
				}
			}
		}

		if (ilesVoisines.isEmpty()) {
			Ile tmpIle = new Ile(cellule);
			iles.add(tmpIle);
		} else {
			ilesVoisines.get(0).ajouterCellule(cellule);
			if (ilesVoisines.size() > 1) {
				fusionnerIles(ilesVoisines);
			}

		}
	}

	/**
	 * Fusionne des iles de meme couleur si elles sont cote � cote.
	 * 
	 * @param ilesVoisines
	 */
	private void fusionnerIles(ArrayList<Ile> ilesVoisines) {
		Ile tmpIle = new Ile();
		Ile ileCourant;

		for (int i = 0; i < ilesVoisines.size(); i++) {
			ileCourant = ilesVoisines.get(i);
			for (Cellule c : ileCourant.getContenu()) {
				tmpIle.getContenu().add(c);
			}
			this.iles.remove(ileCourant);
		}
		this.iles.add(tmpIle);
	}

	public Color getCouleur() {
		return couleur;
	}

	public ArrayList<Ile> getIles() {
		return iles;
	}
}
