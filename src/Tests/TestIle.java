package Tests;

import static org.junit.Assert.*;
import org.junit.Test;

import Hex.Cellule;
import Hex.Ile;

/**
 * Teste les m�thodes principales de la classe Ile
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class TestIle {
	/**
	 * Teste si l'ajout d'une cellule dans une Ile fonctionne
	 */
	@Test
	public void ajoutCelluleDansIle() {
		Ile ile = new Ile();
		assertTrue(ile.getContenu().size() == 0);
		ile.ajouterCellule(new Cellule(5, 5));
		assertTrue(ile.getContenu().size() == 1);
		ile.ajouterCellule(new Cellule(4, 5));
		assertTrue(ile.getContenu().size() == 2);
	}
}
