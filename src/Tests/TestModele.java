package Tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;

import org.junit.Test;

import Hex.Constante;
import Hex.Modele;

/**
 * Teste les m�thodes principales du mod�le.
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class TestModele {
	/**
	 * Teste si le changement de joueur fonctionne
	 */
	@Test
	public void changementJoueur() {
		Modele modele = new Modele();
		assertTrue(modele.getNumeroJoueurCourant() == 0);
		assertTrue(modele.getJoueurCourant().getCouleur() == Constante.couleurJoueur1);

		modele.joueurSuivant();
		assertTrue(modele.getNumeroJoueurCourant() == 1);
		assertTrue(modele.getJoueurCourant().getCouleur() == Constante.couleurJoueur2);

		modele.joueurSuivant();
		assertTrue(modele.getNumeroJoueurCourant() == 0);
		assertTrue(modele.getJoueurCourant().getCouleur() == Constante.couleurJoueur1);
	}

	/**
	 * Teste le placement d'un jeton sur le plateau de jeu (pour les 2 joueurs)
	 */
	@Test
	public void placementJeton() {
		Modele modele = new Modele();
		assertTrue(modele.jouerSiCaseVide(new Point(700, 295)));
		assertTrue(modele.getNumeroJoueurCourant() == 1);

		assertTrue(modele.jouerSiCaseVide(new Point(620, 266)));
		assertTrue(modele.getNumeroJoueurCourant() == 0);

		// La case est d�ja utilis�e, le mod�le ne change pas le num�ro du
		// joueur
		assertFalse(modele.jouerSiCaseVide(new Point(620, 266)));
		assertTrue(modele.getNumeroJoueurCourant() == 0);
	}

	/**
	 * Teste le cas ou un joueur a une ile gagnante
	 */
	@Test
	public void joueurGagne() {
		Modele modele = new Modele();
		modele.jouerSiCaseVide(new Point(713, 338));
		modele.jouerSiCaseVide(new Point(736, 204));
		modele.jouerSiCaseVide(new Point(769, 224));
		modele.jouerSiCaseVide(new Point(672, 169));
		modele.jouerSiCaseVide(new Point(660, 253));
		modele.jouerSiCaseVide(new Point(510, 308));
		modele.jouerSiCaseVide(new Point(506, 284));
		modele.jouerSiCaseVide(new Point(564, 241));
		modele.jouerSiCaseVide(new Point(548, 250));
		modele.jouerSiCaseVide(new Point(583, 269));
		modele.jouerSiCaseVide(new Point(625, 216));
		modele.jouerSiCaseVide(new Point(635, 265));
		modele.jouerSiCaseVide(new Point(654, 279));
		modele.jouerSiCaseVide(new Point(704, 209));
		modele.jouerSiCaseVide(new Point(703, 298));
		modele.jouerSiCaseVide(new Point(740, 236));
		modele.jouerSiCaseVide(new Point(792, 257));
		modele.jouerSiCaseVide(new Point(590, 200));
		modele.jouerSiCaseVide(new Point(608, 183));
		modele.jouerSiCaseVide(new Point(692, 154));
		modele.jouerSiCaseVide(new Point(584, 164));
		modele.jouerSiCaseVide(new Point(500, 190));
		modele.jouerSiCaseVide(new Point(532, 219));
		// La partie n'est pas encore termin�e
		assertTrue(modele.quiGagne() == null);
		modele.jouerSiCaseVide(new Point(500, 234));
		modele.jouerSiCaseVide(new Point(538, 193));
		modele.jouerSiCaseVide(new Point(665, 206));
		modele.jouerSiCaseVide(new Point(700, 251));
		modele.jouerSiCaseVide(new Point(771, 181));
		// La partie n'est pas encore termin�e
		assertTrue(modele.quiGagne() == null);
		modele.jouerSiCaseVide(new Point(745, 287));
		// La partie est termin�e, le joueur 1 (0 dans l'ArrayList) a gagn�
		assertTrue(modele.quiGagne() == modele.getJoueurs().get(0));
	}

	/**
	 * Teste le cas ou une ile d'un joueur relie le meme cot� du plateau (
	 * situation ou le joueur ne doit pas gagner)
	 */
	@Test
	public void mauvaisChemin() {
		Modele modele = new Modele();
		modele.jouerSiCaseVide(new Point(504, 354));
		modele.jouerSiCaseVide(new Point(532, 308));
		modele.jouerSiCaseVide(new Point(538, 335));
		modele.jouerSiCaseVide(new Point(674, 231));
		modele.jouerSiCaseVide(new Point(581, 276));
		modele.jouerSiCaseVide(new Point(614, 224));
		modele.jouerSiCaseVide(new Point(568, 323));
		modele.jouerSiCaseVide(new Point(544, 266));
		modele.jouerSiCaseVide(new Point(583, 250));
		modele.jouerSiCaseVide(new Point(614, 186));
		modele.jouerSiCaseVide(new Point(534, 225));
		modele.jouerSiCaseVide(new Point(501, 201));
		modele.jouerSiCaseVide(new Point(536, 189));
		assertTrue(modele.quiGagne() == null);
	}

	/**
	 * Teste si la partie se termine lorsqu'un pion est de chaque cote de la
	 * grille
	 */
	@Test
	public void unPionDeChaqueCote() {
		Modele modele = new Modele();
		modele.jouerSiCaseVide(new Point(572, 328));
		modele.jouerSiCaseVide(new Point(607, 129));
		modele.jouerSiCaseVide(new Point(657, 233));
		modele.jouerSiCaseVide(new Point(617, 412));
		assertTrue(modele.quiGagne() == null);
	}
}
