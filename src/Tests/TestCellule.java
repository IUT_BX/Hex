package Tests;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import Hex.Cellule;
import Hex.Constante;

/**
 * teste les m�thodes de la classe Cellule
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class TestCellule {
	/**
	 * test des getters
	 */
	@Test
	public void gettersPositionsMatrice() {
		Cellule cellule = new Cellule(5, 6);
		assertEquals(cellule.getPosI(), 5);
		assertEquals(cellule.getPosJ(), 6);
	}

	/**
	 * Teste si la m�thode estVoisine fonctionne
	 */
	@Test
	public void cellulesVoisines() {
		Cellule centre = new Cellule(3, 3);

		Cellule hautGauche = new Cellule(2, 2);
		Cellule hautDroite = new Cellule(4, 3);
		Cellule haut = new Cellule(3, 2);
		Cellule bas = new Cellule(3, 4);
		Cellule basGauche = new Cellule(2, 3);
		Cellule basDroite = new Cellule(4, 4);

		assertTrue(centre.estVoisine(hautGauche));
		assertTrue(centre.estVoisine(hautDroite));
		assertTrue(centre.estVoisine(haut));
		assertTrue(centre.estVoisine(bas));
		assertTrue(centre.estVoisine(basGauche));
		assertTrue(centre.estVoisine(basDroite));
	}

	/**
	 * D�termine que l'algorithme estVoisine ne d�finisse pas 2 cellules non
	 * voisines comme voisines
	 */
	@Test
	public void cellulesNonVoisines() {
		Cellule centre = new Cellule(3, 3);

		Cellule pasAcote1 = new Cellule(10, 4);
		Cellule pasAcote2 = new Cellule(0, 14);

		assertFalse(centre.estVoisine(pasAcote1));
		assertFalse(centre.estVoisine(pasAcote2));
	}

	/**
	 * teste si le changement de joueur fonctionne
	 */
	@Test
	public void modificationCouleur() {
		Cellule cellule = new Cellule(3, 3);
		assertTrue(cellule.getCouleur() == Constante.couleurCaseParDefaut);
		cellule.setCouleur(Color.BLACK);
		assertTrue(cellule.getCouleur() == Color.BLACK);
	}
}
