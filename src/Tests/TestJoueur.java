package Tests;

import static org.junit.Assert.*;
import org.junit.Test;

import Hex.Cellule;
import Hex.Constante;
import Hex.Joueur;

/**
 * Test des m�thodes principales de la Classe Joueur
 * 
 * @author Antoine Poussard & Remi Momprive S3B
 *
 */
public class TestJoueur {
	/**
	 * Teste l'ajout d'une cellule dans les iles du joueur, ainsi que
	 * l'algorithme de fusion des iles.
	 */
	@Test
	public void ajoutCellules() {
		Joueur joueur = new Joueur(Constante.couleurCaseParDefaut);

		// Au debut, il n'y a pas d'iles
		assertTrue(joueur.getIles().size() == 0);

		// Une ile est cr��e
		joueur.ajouterCelluleDansIle(new Cellule(3, 3));
		assertTrue(joueur.getIles().size() == 1);

		// La cellule est ajout�e � l'ile existante
		joueur.ajouterCelluleDansIle(new Cellule(4, 3));
		assertTrue(joueur.getIles().size() == 1);
		// Celle ile contient maintenant deux cellules
		assertTrue(joueur.getIles().get(0).getContenu().size() == 2);

		// On ajoute une cellule; comme cette derni�re n'est pas voisine avec
		// l'ile, on cr�e une autre ile
		joueur.ajouterCelluleDansIle(new Cellule(6, 4));
		assertTrue(joueur.getIles().size() == 2);

		// On place une cellule entre les deux iles, cela fusionne les deux iles
		joueur.ajouterCelluleDansIle(new Cellule(5, 3));
		assertTrue(joueur.getIles().size() == 1);
		assertTrue(joueur.getIles().get(0).getContenu().size() == 4);

	}
}
